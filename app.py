from flask import Flask, request, jsonify, render_template
import tensorflow as tf
from PIL import Image
import io
import base64
import numpy as np
import os

app = Flask(__name__)

# Load the model with more detailed logging
try:
    model = tf.keras.models.load_model('age_classification25.h5')
    app.logger.info("Model loaded successfully.")
except Exception as e:
    app.logger.error(f"Error loading model: {e}")

# Define age ranges
age_ranges = ['11-18', '19-26', '27-34', '35-42', '43-50', '51-58', 'Lesser_Than_11', 'Greater_Than_58', 'Not_HumanFace']

def preprocess_image(image):
    image = image.resize((64,64))
    image = np.array(image)
    if image.shape[-1] == 4:  # Convert RGBA to RGB
        image = image[..., :3]
    image = image / 255.0  # Normalize to [0, 1]
    image = np.expand_dims(image, axis=0)  # Add batch dimension
    return image

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/camera')
def camera():
    return render_template('camera.html')

@app.route('/upload')
def upload():
    return render_template('upload.html')

@app.route('/index')
def home_page():
    return render_template('index.html')

@app.route('/predict-age', methods=['POST'])
def predict_age():
    try:
        data = request.get_json()
        app.logger.info(f"Received data: {data}")

        if not data:
            return jsonify({'error': 'No data provided'}), 400
        
        if 'image' not in data:
            app.logger.error(f"Missing 'image' key in data: {data}")
            return jsonify({'error': 'Missing image key in data'}), 400

        image_data = data['image'].split(',')[1]  # Get base64 part of the image

        image = Image.open(io.BytesIO(base64.b64decode(image_data)))

        processed_image = preprocess_image(image)

        predictions = model.predict(processed_image)
        app.logger.info(f"Prediction result: {predictions}")
        predicted_age_range_index = np.argmax(predictions[0])
        predicted_age_range = age_ranges[predicted_age_range_index]

        return jsonify({'predicted_age_range': predicted_age_range})

    except Exception as e:
        app.logger.error(f"Error occurred: {e}")
        return jsonify({'error': 'An internal error occurred'}), 500

if __name__ == '__main__':
    port = int(os.environ.get("PORT", 5000))
    app.run(host="0.0.0.0", port=port, debug=True)
